import { NgModule } from '@angular/core';

import { Jhipster180407SharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
  imports: [Jhipster180407SharedLibsModule],
  declarations: [JhiAlertComponent, JhiAlertErrorComponent],
  providers: [],
  exports: [Jhipster180407SharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class Jhipster180407SharedCommonModule {}
